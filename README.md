### SQLAlchemy training

It runs on both Python 2.7 and 3.5.

## Installation

First, clone the repository and create a virtualenv. Then install the requirements:

`$ pip install -r requirements.txt`


Finally you can run the application:

`./manage.py runserver`

or play in the Python REPL:

`./manage.py shell`

To run the tests

`./manage.py test`

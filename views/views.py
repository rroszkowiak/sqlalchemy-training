# -*- coding=utf-8 -*-
from flask import Blueprint, render_template

from .wine_conference import get_wine_conf_data


website_app = Blueprint('website_app', __name__)


@website_app.route('/')
def index():
    return render_template('website_app/index.html')


@website_app.route('/agenda')
def agenda():
    return render_template('website_app/agenda.html')


@website_app.route('/speakers')
def speakers():
    return render_template('website_app/speakers.html')


@website_app.route('/wine-conference')
def wine_conference():
    wine_conference_data = get_wine_conf_data()
    return render_template('website_app/wine_conference.html', **wine_conference_data)


@website_app.route('/whisky-conference')
def whisky_conference():
    return render_template('website_app/whisky_conference.html')

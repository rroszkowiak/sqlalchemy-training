# -*- coding=utf-8 -*-

def get_wine_conf_data():
    return {
        'agenda': get_agenda(),
        'talks': get_talks(),
        'speakers': get_speakers()
    }


def get_speakers():
    return ['Adam White', 'Barry Gold', 'Kate Brown']


def get_talks():
    return ['Some talk 1', 'Some other talk', 'Amazing talk']


def get_agenda():
    return ['First slot talk', 'second slot talk']

# -*- coding=utf-8 -*-
DEBUG = True

ERROR_404_HELP = False

# Of course keeping credentials in the VCS tracked file is not a good idea!
# Dont't do it unless it's a training app running on localhost
MAIN_DB = {
    'host': '',
    'user': '',
    'password': '',
    'db_name': '',
    'protocol': ''
}

WINE_DB = {
    'host': 'localhost',
    'user': 'pyconpl',
    'password': 'sqlalchemy_training',
    'db_name': 'wine_conference',
    'protocol': 'mysql+pymysql'
}

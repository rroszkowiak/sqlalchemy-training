#-*- coding=utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from config.local import DEBUG
from config.local import WINE_DB

ENGINE_URI_PATTERN = '{protocol}://{user}:{password}@{host}/{db_name}'

wine_db_uri = ENGINE_URI_PATTERN.format(
    protocol=WINE_DB['protocol'],
    host=WINE_DB['host'],
    user=WINE_DB['user'],
    password=WINE_DB['password'],
    db_name=WINE_DB['db_name']
)

wine_db_engine = create_engine(wine_db_uri, pool_size=20, max_overflow=10, echo=DEBUG, echo_pool=DEBUG)

wine_db_session = scoped_session(
    sessionmaker(bind=wine_db_engine, autocommit=False, autoflush=False)
)
